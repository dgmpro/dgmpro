<?php
class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users_model');
	}
	
	public function index(){
		$this->output->enable_profiler(TRUE);
		$data['users'] = $this->users_model->get_users();
		$data['title'] = 'Users List';
		$this->load->view('templates/header', $data);
		$this->load->view('users/index', $data);
		$this->load->view('templates/footer');
	}

	public function view($slug)
	{		
		$data['users_item'] = $this->users_model->get_users($slug);	
	
		if (empty($data['users_item']))
		{
			show_404();
		}
	
		$data['title'] = $data['users_item']['Usr_id'];
	
		$this->load->view('templates/header', $data);
		$this->load->view('users/view', $data);
		$this->load->view('templates/footer');	
	}
}	
?>