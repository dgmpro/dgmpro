<?php
class Users_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}


	public function get_users($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('SD_user',20);
			return $query->result_array();
		}		
		$query = $this->db->get_where('SD_user', array('Usr_id' => $slug));
		return $query->row_array();
	}
}
?>